"use strict";

angular.module('app',[
	'ngMaterial'
])

.config(['$httpProvider', function($httpProvider){
	$httpProvider.defaults.useXDomain = true;
}])

.controller('mainCtrl', ['$scope', 'factory', '$filter', '$http', 
	function($scope, factory, $filter, $http){
	
	$scope.header = 'CRUD APP EXAMPLE';

	//$scope.filterText = 'meno';

	var getUsers = $http({
		method	: "GET",
		url		: 'http://localhost:4000/user',
	})

	console.log(getUsers);
	this._users = []

	getUsers.then(function(response){
		$scope.users = response.data.users;
	})

	this.addItem = function(){
		console.log('addItem');
		factory.addItem($scope.newItem).then(function(resp){
			if(resp){
				factory.load().then(function(response){
					$scope.users = response.data.users;
				})
			}
		});
	}

	this.editItem = function(index){
		if ($scope.itemEdited == index){
			$scope.itemEdited = null;
		} else {$scope.itemEdited = index;}
	}

	this.removeItem = function(item) {
		factory.removeItem(item._id).then(function(resp){
			console.log(resp);
			if (resp.success){
				factory.load().then(function(response){
					$scope.users = response.data.users;
				})
			}
		});
	}

	this.saveItem = function(item){
		factory.updateItem(item);
		$scope.itemEdited = null;
	}

	$scope.newItem = {};

	var orderBy = $filter('orderBy');
	$scope.orderItems = function(predicate) {
		$scope.predicate = predicate;
		$scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
		$scope.items = orderBy($scope.items, predicate, $scope.reverse);
	}

	$scope.orderItems('id');

	this.count = 4;
}])

.directive('addUser', function() {
  return {
    restrict: 'A',
    scope: {
    	userbind: '='
    },
    controller: function($scope, factory, $http, $timeout){
    	$timeout(function() {console.log($scope.userbind);}, 1500);

    	$scope.newItem = {};

		$scope.addItem = function(){
			factory.addItem($scope.newItem).then(function(resp){
				if(resp){
					factory.load().then(function(response){
						$scope.userbind = response.data.users;
					})
				}
			});
		}
    },
    templateUrl: 'tmp/dir.html'
  };
})

.component('addUser', {
	bindings: {
		users: '='
	},
	controller:  'compController as ctrl2' ,
	templateUrl: 'tmp/comp.html'
})

.controller('compController', [ '$timeout', 'factory',  function ($timeout, factory) {
	this.newItem = {};
	this.addItem = function(){
		factory.addItem(this.newItem).then(function(resp){
			if(resp){
				factory.load().then(function(response){
					$scope.users = response.data.users;
				})
			}
		});
		
	}
}])

.factory('factory', ['$http', function($http){
	var service = {};

	service.load = function(){
		return $http({
			method	: "GET",
			url		: 'http://localhost:4000/user',
		}).success(function(response){
			console.log(response);
			return response.users
		}, function(err){
			console.log(err);
		});
	};

	service.addItem = function(obj){
		console.log(obj);
		return $http({
			method	: "post",
			data	: {
				id 		: obj.id,
				name 	: obj.name,
				salary 	: obj.salary,
				color 	: obj.color,
				email	: obj.email
			},
			url: 'http://localhost:4000/user',
		}).success(function(response){
			return response.data
		}, function(err){
			console.log(err);
		});
	};

	service.removeItem = function(_id){
		return $http({
			method	: "delete",
			url 	: 'http://localhost:4000/user/' + _id,
		}).then(function(response){
			console.log(response.data);
			return response.data
		});
	};

	service.updateItem = function(item){
		console.log(item);
		return $http({
			method	: "put",
			url		: 'http://localhost:4000/user/' + item._id,
			data 	: {
				id: item.id,
				name: item.name,
				email: item.email,
				salary: item.salary,
				color: item.color
			},
		}).then(function(response){
			console.log(response.data);
			return response.data
		});
	};

	return service;
}])